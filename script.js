'use strict';

window.onload = init;

function init() {
    document.getElementById('oeil').addEventListener('click', visibilite);
    document.getElementById('mdp').addEventListener('input', verifierMdP);
    document.getElementById('oeil2').addEventListener('click', visibilite2);
    document.getElementById('name').addEventListener('input', nameTailleMini);
    document.getElementById('mail').addEventListener('input', verifierMail);
    //document.getElementById('mdpOk').addEventListener('input',verifSaisieMdp)
}

function visibilite() {
    let mdp = document.getElementById('mdp');
    if (mdp.type === 'text') {
        mdp.type = 'password';
        document.getElementById('oeil').src = 'eye-closed.png';
    } else {
        mdp.type = 'text';
        document.getElementById('oeil').src = 'eye-open.png';
    }
}
function visibilite2() {
    let mdpOk = document.getElementById('mdpOk');
    if (mdpOk.type === 'text') {
        mdpOk.type = 'password';
        document.getElementById('oeil2').src = 'eye-closed.png';
    } else {
        mdpOk.type = 'text';
        document.getElementById('oeil2').src = 'eye-open.png';
    }
}

/*function verifierName(event){
let saisieName = event.currentTarget.value;
let tailleMiniOk =  verifierTailleMini (saisieName);
 if (tailleMiniOk) {
    document.getElementById('ok').src = 'Projet\check.svg';
 } else {
    document.getElementById('error').src = 'Projet\error.svg';
 }
}*/

function verifNom(){
let nameOk = nameTailleMini(saisie);
if (nameOk) {
    document.getElementById('ok').style.display = block;
 } else {
    document.getElementById('error').style.display = none;
 }
}

function verifierMail(){
 let mailOk = verifierMail(saisie);
  if (mailOk) {
    document.getElementById('ok').style.display = hidden;
 } else {
    document.getElementById('error').style.display = block;
 }
}




function verifierMdP(event) {
    let saisie = event.currentTarget.value; // le contenu du champ input
    
    // controle des differentes contraintes
    
   
    let symboleOK = verifierSymbole(saisie);
    let chiffresOK = verifierChiffre(saisie);
    let tailleOK = verifierTaille(saisie);
    let saisieOk = verifSaisieMdp(mdp.value, mdpOk.value);
    // application des regles d'affichage en fonction des contraintes respectees ou non
   
    if (symboleOK) {
        let consigneSymbole = document.getElementById('symbole');
        passerVert(consigneSymbole);
    } else {
        let consigneSymbole = document.getElementById('symbole');
        passerRouge(consigneSymbole);
    }

    if (chiffresOK) {
        let consigneChiffre = document.getElementById('chiffre');
        passerVert(consigneChiffre);
    } else {
        let consigneChiffre = document.getElementById('chiffre');
        passerRouge(consigneChiffre);
    }

    if (tailleOK) {
        let consigneTaille = document.getElementById('nbCaracteres');
        passerVert(consigneTaille);
    } else {
        let consigneTaille = document.getElementById('nbCaracteres');
        passerRouge(consigneTaille);
    }

   if (saisieOk) {
        let consigneSaisie2 = document.getElementById('saisie2');
        passerVert(consigneSaisie2);
    } else {
        let consigneSaisie2 = document.getElementById('saisie2');
        passerRouge(consigneSaisie2);
    }

    if (nameOk && mailOk && symboleOK && chiffresOK && tailleOK && saisieOk) {
        document.getElementById('valider').disabled = false;
    } else {
        document.getElementById('valider').disabled = true;
    }
}

function nameTailleMini(saisie) {
    return saisie.length >= 3;
}
function verifierMail(saisie) {
    return saisie.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g) != null;
}


function passerVert(element) {
    element.style.color = 'green';
}

function passerRouge(element) {
    element.style.color = 'red';
}

function verifierSymbole(saisie) {
   return saisie.match(/[#-*]/g) != null;
}

function verifierChiffre(saisie) {
    return saisie.match(/[0-9]/g) != null;
}

function verifierTaille(saisie) {
    return saisie.length >= 6;
}

// function pour l'image qui sera en hidden ou pas 
function imgCheck (){
    element.src = 'check.svg'
  let element = element.getElementById('check');
  if (element===true) {
    element.style.display = hidden;

  }
}

function verifSaisieMdp(mdp, mdpOk) {
        let saisieOk = false
        if ( mdp.value === mdpOk.value){
            saisieOk = true;
        }    
    return saisieOk;
}

    



