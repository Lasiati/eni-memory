"use strict";

window.onload = init;

let Images = [
  "image/saitama.jpg",
  "image/bojji2.jpg",
  "image/kakashi.jpg",
  "image/saitama.jpg",
  "image/goku.jpg",
  "image/lufy1.jpg",
  "image/goku.jpg",
  "image/bojji2.jpg",
  "image/lufy1.jpg",
  "image/denji.jpg",
  "image/kakashi.jpg",
  "image/denji.jpg",
];

let cartes;
let passage = 0; // pour pouvoir comptabiliser le nombre de click
let carte1; //pour stocker la valeur de ma carte 1
let carte2; //pour stocker la valeur de ma carte 2
let img1; // pour stocker la valeur de mon image 1
let img2; //pour stocker la valeur de mon image 2
let coup = 0;
let score =2000; 
let nbPaires = 6;//Images.length /2;
let pairesTrouvees= 0;


function init() {
  cartes = [
    document.getElementById("carte0"),
    document.getElementById("carte1"),
    document.getElementById("carte2"),
    document.getElementById("carte3"),
    document.getElementById("carte4"),
    document.getElementById("carte5"),
    document.getElementById("carte6"),
    document.getElementById("carte7"),
    document.getElementById("carte8"),
    document.getElementById("carte9"),
    document.getElementById("carte10"),
    document.getElementById("carte11"),
  ];

  cartes.forEach((image) => {
    image.addEventListener("click", retourner);
  });

  const plateau = document.querySelector(".plateau");
  for (let i = cartes.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [cartes[i], cartes[j]] = [cartes[j], cartes[i]];
    cartes.forEach((carte) => plateau.appendChild(carte));
  }
}

function customAlert(score, coup) {
  var modal = document.createElement('div');
  modal.style.position = 'fixed';
  modal.style.top = '50%';
  modal.style.left = '50%';
  modal.style.transform = 'translate(-50%, -50%)';
  modal.style.background = 'black';
  modal.style.fontFamily = 'arial';
  modal.style.padding = '20px';
  modal.style.border = '2px solid black';
  modal.style.borderRadius = '5px';
  var content = document.createElement('p');
  content.innerText = "Yoou Winn copainssss !!! Ton score est de " + score + ". Tu l'as fini en seulement " + coup + " coups.";
  var closeBtn = document.createElement('button');
  closeBtn.innerText = 'Ok !! ';
  closeBtn.style.padding="auto";
  closeBtn.style.fontFamily='arial';
  closeBtn.style.margin = '0 auto';
  closeBtn.style.marginTop = '20px';
  closeBtn.style.display = 'block';
  closeBtn.style.background = 'green';
  closeBtn.onclick = function() {
    modal.remove();
  };
  modal.appendChild(content);
  modal.appendChild(closeBtn);
  document.body.appendChild(modal);
}

function retourner(image) {
  if(passage === 2) return;
  // On récupère la valeur numérique de l'id de l'image
  // "image0" --> "0" --> 0
  let position = parseInt(image.currentTarget.id.substring(5));

  // Permet de modifier la source de l'image sur laquelle on clique
  image.currentTarget.src = Images[position];

  if (passage === 0) {
    carte1 = image.currentTarget;
    carte1.classList.toggle("flipcarte");
    passage++;
    coup++;
    score -=25;
    img1 = carte1.src;
    carte1.removeEventListener("click", retourner);
    } else if (passage === 1) {
    carte2 = image.currentTarget;
    carte2.classList.toggle("flipcarte");
    img2 = carte2.src;
    passage++;
    carte1.removeEventListener("click", retourner);
    compare(img1, img2, carte1, carte2);
  
    }
  }
    
function compare (img1, img2 , carte1, carte2){
  
  if (img1 === img2) {
      carte1.removeEventListener("click", retourner);
      carte2.removeEventListener("click", retourner);
      passage = 0;
      pairesTrouvees++;
      if (pairesTrouvees === nbPaires)
      setTimeout(function () {
        
      customAlert(score, coup);
  relanceGame();},1000);
    
    } else {
      
      setTimeout(function () {
        carte1.classList.toggle("flipcarte");
        carte1.src = "flag.jpg";
        carte2.src = "flag.jpg";
        carte2.classList.toggle("flipcarte");
        carte1.addEventListener("click", retourner);
        carte2.addEventListener("click", retourner);
        passage = 0;
      }, 2000);
    }
}



function relanceGame (){

document.addEventListener('keydown',function(Espace){
   if (Espace.code !== 'Space') {
      }else{
       window.location.reload() 
   }
  })
}
